locals {
  security_group_rules = distinct(concat(
    [for item in var.security_group_rules : replace(replace(item, " immutable", ""), " mutable", "")],
    [for item in var.config_security_group_rules : replace(replace(item, " immutable", ""), " mutable", "")]
  ))
}

resource "openstack_networking_network_v2" "network" {
  name           = "${var.workspace_id}-private-network"
  admin_state_up = "true"
  tags = [
    "${var.workspace_id}-private-network",
    var.workspace_id,
    var.subscription,
    var.application_type,
    var.resource_type,
    var.cloud_type,
    var.subscription_group,
  ]
}

resource "openstack_networking_subnet_v2" "subnet" {
  name            = "${var.workspace_id}-subnet"
  network_id      = openstack_networking_network_v2.network.id
  cidr            = var.cidr
  ip_version      = 4
  dns_nameservers = var.nameservers
  tags = [
    "${var.workspace_id}-subnet",
    var.workspace_id,
    var.subscription,
    var.application_type,
    var.resource_type,
    var.cloud_type,
    var.subscription_group,
  ]
}

resource "openstack_networking_router_v2" "router" {
  name                = "${var.workspace_id}-router"
  external_network_id = var.router_external_network_id
  tags = [
    "${var.workspace_id}-router",
    var.workspace_id,
    var.subscription,
    var.application_type,
    var.resource_type,
    var.cloud_type,
    var.subscription_group,
  ]


}

resource "openstack_networking_router_interface_v2" "router_interface" {
  router_id = openstack_networking_router_v2.router.id
  subnet_id = openstack_networking_subnet_v2.subnet.id
}
