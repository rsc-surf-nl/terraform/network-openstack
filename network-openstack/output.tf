output "id" {
  value = openstack_networking_network_v2.network.id
}

output "network_id" {
  value = openstack_networking_network_v2.network.id
}

output "network_name" {
  value = openstack_networking_network_v2.network.name
}

output "subnet_id" {
  value = openstack_networking_subnet_v2.subnet.id
}

output "router_id" {
  value = openstack_networking_router_v2.router.id
}

output "subnet_cidr" {
  value = openstack_networking_subnet_v2.subnet.cidr
}

output "description" {
  value = var.description
}

output "secgroup_rules" {
  value = local.security_group_rules
}

# Mark support for individual NSGs
output "terraform_script_version" {
  value = 2.1
}

