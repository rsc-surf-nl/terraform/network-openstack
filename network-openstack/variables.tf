variable "user_name" {}
variable "password" {}
variable "project_id" {}
variable "project_name" {}
variable "region" {}
variable "auth_url" {}
variable "network_name" {}
variable "description" {}
variable "cidr" {
  type    = string
  default = "10.10.10.0/24"
}
variable "router_external_network_id" {}
variable "security_group_rules" {
  type = list(string)
}
variable "config_security_group_rules" {
  type    = list(string)
  default = []
}
variable "nameservers" {
  type        = list(string)
  description = "A list of nameservers to use"
}

# SRC automatic variables are added explicitly to be self-contained
variable "application_type" {
  type        = string
  description = "The application type (eg. 'Compute', 'Storage' etc.)"
  default     = "Network"
}
variable "cloud_type" {
  type        = string
  description = "Cloud type (eg. 'AWS', 'Openstack', 'Azure' etc.)"
  default     = "Openstack"
}
variable "co_id" {
  type        = string
  description = "The ID of the associated CO"
  default     = "unset"
}
variable "resource_type" {
  type        = string
  description = "The resource type (eg. 'VM', 'Storage-Volume' etc.)"
  default     = "Private-Network"
}
variable "subscription" {
  type        = string
  description = "The (SRC) subscription under which this resources is created"
  default     = "unset"
}
variable "subscription_group" {
  type        = string
  description = "The subscription group to which the `subscription` belongs"
  default     = "unset"
}
variable "wallet_id" {
  type        = string
  description = "The ID of the associated wallet"
  default     = "unset"
}
variable "workspace_fqdn" {
  type        = string
  description = "The FQDN assigned to this workspace"
  default     = "example.surf-hosted.nl"
}
variable "workspace_id" {
  type        = string
  description = "The ID of this workspace"
  default     = "unset"
}
